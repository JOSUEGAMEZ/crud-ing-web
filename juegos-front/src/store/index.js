import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    juegos: [],
    juego: {},
    loading: false
  },
  mutations: {
    SET_JUEGOS(state, juegos) {
      state.juegos = juegos;
    },
    SET_LOADING(state, newValue) {
      state.loading = newValue;
    },
    SET_JUEGO(state, juego) {
      state.juego = juego;
    },
  },
  actions: {
    listar({commit}) {
      commit('SET_LOADING', true);
      axios.get('http://localhost:3000/game')
      .then(res => commit('SET_JUEGOS', res.data))
      .finally(() =>  commit('SET_LOADING', false));
    },
    
    agregarJuego({commit}, {params, onComplete, onError}){
      axios.post('http://localhost:3000/game', params)
      .then(onComplete)
      .catch(onError);
    },
    obtenerJuego({commit}, {id, onComplete, onError}) {
      axios.get(`http://localhost:3000/game/${id}`)
      .then(res => {
        commit('SET_JUEGO', res.data.result);
        onComplete(res);
      })
      .catch(onError);
    },
    editarJuego({commit}, {id, params, onComplete, onError}){
      axios.put(`http://localhost:3000/game/${id}`, params)
      .then(onComplete)
      .catch(onError)
    },  
    eliminarJuego({commit}, {id, onComplete, onError}){
      axios.delete(`http://localhost:3000/game/${id}`)
      .then(onComplete)
      .catch(onError);
    },
  },
  modules: {
  }
})
