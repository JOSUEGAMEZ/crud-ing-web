import Vue from 'vue'
import VueRouter from 'vue-router'
import Visualizar from '../views/Visualizar.vue'
import Agregar from '../views/Agregar.vue'
import Editar from '../views/Editar.vue'




Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Visualizar',
    component: Visualizar
  },
  {
    path: '/agregar',
    name: 'Agregar',
    component: Agregar
  },
  {
    path: '/editar/:id',
    name: 'Editar',
    component: Editar
  }
 
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
