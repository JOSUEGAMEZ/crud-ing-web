const { CardPlugin } = require('bootstrap-vue');
const connection = require('../config/connections');

function listar(req, res) {
    if(connection) {
        
        let sql = "SELECT * FROM games";

        connection.query(sql, (err, data) => {
            if(err) {
                res.status(400).json(err);
            } else {
                res.json(data);
            }
        })
    }
}


function obtenerJuego(req, res) {
    if(connection){
        const {id} = req.params;

        let sql = `SELECT * FROM games WHERE idgame = ${connection.escape(id)}`;

        connection.query(sql, (err, games) => {
            if(err) {
                res.status(400).json(err);
            } else {
                let mensaje = "";
                if(games === undefined || games.length === 0)
                    mensaje="games no encontrada";

                res.json({result: games[0], mensaje});
            }
        });
    }
}

function crear(req, res) {
    if(connection){
        console.log(req.body);
        const games = req.body;

        if(!games.nombre){
            return res.status(400).send({error: true, mensaje: "El nombre es obligatorio"});
        }
        if(!games.publisher){
            return res.status(400).send({error: true, mensaje: "El publisher es obligatorio"});
        }

        if(!games.category){
            return res.status(400).send({error: true, mensaje: "El cateogry es obligatorio"});
        }

        if(games.nombre.length > 80 && games.nombre) {
            return res.status(400).send({error: true, mensaje: "La longitud debe de ser de 80 caracteres."});
        }
       
        if(games.publisher.length > 60 && games.publisher) {
            return res.status(400).send({error: true, mensaje: "La longitud debe de ser de 60 caracteres."});
        }
        if(games.category.length > 2 && games.category) {
            return res.status(400).send({error: true, mensaje: "La longitud debe de ser de 2 caracteres."});
        }
        if(games.description.length > 200 && games.description) {
            return res.status(400).send({error: true, mensaje: "La longitud debe de ser de 200 caracteres."});
        }
        if(games.year.length > 4 && games.year) {
            return res.status(400).send({error: true, mensaje: "La longitud debe de ser de 4 caracteres."});
        }

    let sql = "INSERT INTO games set ?";

        connection.query(sql, [games], (err, data) => {
            if(err) {
                res.status(400).json(err);
            } else {
                res.json({error: false, result: data, mensaje: "games creada con éxito."})
            }
        })
    }
}

function editar(req, res) {
    if(connection) {
        const { id } = req.params;
        const games = req.body;
        
       
        if(!games.publisher){
            return res.status(400).send({error: true, mensaje: "El publisher es obligatorio"});
        }

        if(!games.category){
            return res.status(400).send({error: true, mensaje: "El cateogry es obligatorio"});
        }

        if(games.publisher.length > 60 && games.publisher) {
            return res.status(400).send({error: true, mensaje: "La longitud debe de ser de 60 caracteres."});
        }
        if(games.category.length > 2 && games.category) {
            return res.status(400).send({error: true, mensaje: "La longitud debe de ser de 2 caracteres."});
        }
        if(games.description.length > 200 && games.description) {
            return res.status(400).send({error: true, mensaje: "La longitud debe de ser de 200 caracteres."});
        }
        if(games.year.length > 4 && games.year) {
            return res.status(400).send({error: true, mensaje: "La longitud debe de ser de 4 caracteres."});
        }


        let sql = "UPDATE games set ? WHERE idgame = ?";

        connection.query(sql, [games, id], (err, data) => {
            if(err) {
                res.status(400).json(err);
            } else {
                let mensaje = "";
                if(data.changedRows === 0)
                    mensaje ="La información es la misma";
                else
                    mensaje ="games actualizada con exito"
                res.json({error: false, result: data, mensaje});
            }
        });
    }
}

function eliminar(req, res){
    if(connection) {
        const {id} = req.params;

        let sql = "DELETE FROM games WHERE idgame = ?";
        connection.query(sql, [id], (err, data) => {
            if(err){
                res.status(400).json(err);
            } else {
                let mensaje = "";
                if(data.affectedRows === 0)
                    mensaje = "games no encontrada";
                else
                    mensaje = "games eliminada con éxito.";
                res.json({error: false, result: data, mensaje});
            }
        });

    }
}

module.exports = {
    listar, //Read
    obtenerJuego,  
    crear, //Create
    editar, //Update
    eliminar //Delete
}

