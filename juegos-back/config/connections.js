//establecer conexion a bd
const mysql = require('mysql');

//establecer host
const objectConnection = {
    "host": "localhost",
    "port": 3306,
    "user": "root",
    "password": "0000",
    "database": "crud_juego"
}

// creas la conexion con el objeto de conexion
const myConn = mysql.createConnection(objectConnection);

// conectarse a la BD  con la constante myConn
myConn.connect((error) => {
    if(error) {
        console.log("Ha ocurrido un error: ", error);
    } else {
        console.log("Base de datos conectada con exito");
    }
});

module.exports = myConn;

