create database crud_juego;
use crud_juego;


CREATE TABLE games(
	idgame INT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(80) NOT NULL,
    publisher VARCHAR(60) NOT NULL,
    category VARCHAR(2) NOT NULL,
    description VARCHAR(200),
    year VARCHAR(4),
    PRIMARY KEY(idgame)
);

insert into games(nombre,publisher,category,description,year)
values('ARK survival','Savage','11','Juego De Supervivencia Extrema','2018');

insert into games(nombre,publisher,category,description,year)
values('Forza Horizon 5','Turn 10 Studios Playground Games Sumo Digital','16','Juego de simulacion de conduccion de coches','2022');



