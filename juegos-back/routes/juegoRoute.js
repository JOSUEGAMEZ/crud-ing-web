const express = require('express');
const { route } = require('express/lib/application');

const routes = express.Router();

const { listar, obtenerJuego,crear, editar, eliminar } = require('../controllers/juegoControllers');

routes.get('/game', listar);

routes.get('/game/:id', obtenerJuego );

routes.post('/game', crear);

routes.put('/game/:id', editar);

routes.delete('/game/:id', eliminar);




module.exports = routes;